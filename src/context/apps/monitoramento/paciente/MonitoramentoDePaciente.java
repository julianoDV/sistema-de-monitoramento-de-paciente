package context.apps.monitoramento.paciente;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.PanelUI;

import context.arch.discoverer.Discoverer;
import context.arch.enactor.Enactor;
import context.arch.enactor.EnactorXmlParser;
import context.arch.widget.Widget;
import context.arch.widget.WidgetXmlParser;

/**
 * Main class for Hello World context-aware application of a living room smart lamp.
 * It demonstrates four steps to create a simple context-aware application:
 * <ol>
 * 	<li>Modeling context with Widgets</li>
 * 	<li>Modeling reasoning with Enactors</li>
 * 	<li>Modeling behavior with Services</li>
 * 	<li>Combining models</li>
 * </ol>
 * Running this program would launch a GUI simulation of a room sensor suite
 * with brightness and presence sensors, and lamp light level.
 * 
 * @author Brian Y. Lim
 *
 */
public class MonitoramentoDePaciente {
	
	protected Widget pacienteWidget;//roomWidget (atributo de entrada)
	protected Widget emailWidget;//lightWidget (atributo de saida)

	protected Enactor enactor;
	
	protected EmailService emailService; // lightService
	protected SendMail sendMail;
	
	protected MonitoramentoDePacienteUI ui;//HelloRoomUI
	static JFrame paciente;
	

	public MonitoramentoDePaciente() {
		super();
		
		sendMail = new SendMail();
		
		/*
		 * Widget do Paciente
		 */
		pacienteWidget = WidgetXmlParser.createWidget("monitoramento/paciente/paciente-widget.xml");
		
		/*
		 * Envio de Email e Widget de sa�da
		 */
		emailWidget = WidgetXmlParser.createWidget("monitoramento/paciente/email-widget.xml");
		emailService = new EmailService(emailWidget);
		emailWidget.addService(emailService);
		
		/*
		 * Enactor
		 */
		enactor = EnactorXmlParser.createEnactor("monitoramento/paciente/room-enactor.xml");

		// setup UI component
		ui = new MonitoramentoDePacienteUI(emailService.emailLabel); // need to attach lightService before starting
	}
	

	/**
	 * Class to represent the UI representation for the application.
	 * @author Brian Y. Lim
	 */
	@SuppressWarnings("serial")
	public class MonitoramentoDePacienteUI extends JPanel {
		
		private JSlider pressaoSlider;//brightnessSlider
		private JSlider glicoseSlider;
		private JSlider batimentoSlider;
		private JSpinner presencaSpinner;//presenceSpiner
		private float fontSize = 20f;
		
		Date d = GregorianCalendar.getInstance().getTime();
		SimpleDateFormat format = new SimpleDateFormat();
		
		public MonitoramentoDePacienteUI(JLabel emailLabel) {			
			setLayout(new GridLayout(5, 1)); // 5 rows, 1 columns
			
			/*
			 * UI for brightness
			 */
			add(new JLabel("Pressao") {{ setFont(getFont().deriveFont(fontSize)); }});
			
			add(pressaoSlider = new JSlider(new DefaultBoundedRangeModel(160, 0, 0, 255)) {{
				addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent evt) {
						// brightness is from 0 to 255
						
						
						
						short pressao = (short)pressaoSlider.getValue();
						short batimento = (short)batimentoSlider.getValue();
						short glicose = (short)glicoseSlider.getValue();
						pacienteWidget.updateData("pressao", pressao);
						int presenca = (Integer) presencaSpinner.getValue();
						String nome = paciente.getTitle();
						if(presenca == 1){
							if(pressao%10 == 0){
								if(batimento < 150 && pressao < 150 && glicose < 150){
									new Thread();
									try {
										Thread.sleep(5000);
										System.out.println("Enviando Email...");
										try {
											sendMail.generateAndSendEmail(nome,batimento,glicose,pressao);
										} catch (AddressException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (MessagingException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (SQLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								}
								
								System.out.print("[");
								System.out.print(format.format(d));
								System.out.print("] ");
								System.out.println("PRESS�O ARTERIAL = " + pressao + " | BATIMENTOS CARD�ACOS = " +batimento+" | N�VEL DE GLICOSE = " +glicose);
							}
						}
						if(pressao%20 == 0){
							ConexaoMySQL con = new ConexaoMySQL();
							con.getDBConnection();							
							try {
								
								con.insertRecordIntoTable(nome,batimento,glicose,pressao);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								con.FecharConexao();
							}
							con.FecharConexao();
							
						}
						// set color to represent brightness level
						setBackground(new Color(pressao,pressao,pressao));
					}
				});
				setOpaque(true); // to allow background color to show
				setMajorTickSpacing(50); // escala do slider
				setPaintTicks(true); // tracos slider
				setPaintLabels(true); // numeros slider
			}});
			
			add(new JLabel("Glicose") {{ setFont(getFont().deriveFont(fontSize)); }});
			
			add(glicoseSlider = new JSlider(new DefaultBoundedRangeModel(80, 0, 0, 255)) {{
				addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent evt) {
						// brightness is from 0 to 255
						short pressao = (short)pressaoSlider.getValue();
						short batimento = (short)batimentoSlider.getValue();
						short glicose = (short)glicoseSlider.getValue();
						pacienteWidget.updateData("glicose", glicose);
						int presenca = (Integer) presencaSpinner.getValue();
						String nome = paciente.getTitle();
						if(presenca == 1){
							if(glicose%10 == 0){
								if(batimento < 150 && pressao < 150 && glicose < 150){
									new Thread();
									try {
										Thread.sleep(5000);
										System.out.println("Enviando Email...");
										try {
											sendMail.generateAndSendEmail(nome,batimento,glicose,pressao);
										} catch (AddressException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (MessagingException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (SQLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								}
							
								System.out.print("[");
								System.out.print(format.format(d));
								System.out.print("] ");
								System.out.println("PRESS�O ARTERIAL = " + pressao + " | BATIMENTOS CARD�ACOS = " +batimento+" | N�VEL DE GLICOSE = " +glicose);
							}
						}
						
						if(pressao%20 == 0){
							ConexaoMySQL con = new ConexaoMySQL();
							con.getDBConnection();							
							try {
								
								con.insertRecordIntoTable(nome,batimento,glicose,pressao);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								con.FecharConexao();
							}
							con.FecharConexao();
							
						}
						
						// set color to represent brightness level
						setBackground(new Color(glicose,glicose,glicose));
					}
				});
				setOpaque(true); // to allow background color to show
				setMajorTickSpacing(50);
				setPaintTicks(true);
				setPaintLabels(true);
			}});
			
			add(new JLabel("Batimento"){{ setFont(getFont().deriveFont(fontSize)); }} );
			
			add(batimentoSlider = new JSlider(new DefaultBoundedRangeModel(140, 0, 0, 255)) {{
				addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent evt) {
						// brightness is from 0 to 255
						
						short pressao = (short)pressaoSlider.getValue();
						short batimento = (short)batimentoSlider.getValue();
						short glicose = (short)glicoseSlider.getValue();
						int presenca = (Integer) presencaSpinner.getValue();
						String nome = paciente.getTitle();
						if(presenca == 1){
							if(batimento%10 == 0){
								if(batimento < 150 && pressao < 150 && glicose < 150){
									new Thread();
									try {
										Thread.sleep(5000);
										System.out.println("Enviando Email...");
										try {
											sendMail.generateAndSendEmail(nome,batimento,glicose,pressao);
										} catch (AddressException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (MessagingException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (SQLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								}
								System.out.print("[");
								System.out.print(format.format(d));
								System.out.print("] ");
								System.out.println("PRESS�O ARTERIAL = " + pressao + " | BATIMENTOS CARD�ACOS = " +batimento+" | N�VEL DE GLICOSE = " +glicose);
							}
						}
						
						if(pressao%20 == 0){
							ConexaoMySQL con = new ConexaoMySQL();
							con.getDBConnection();							
							try {
								
								con.insertRecordIntoTable(nome,batimento,glicose,pressao);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								con.FecharConexao();
							}
							con.FecharConexao();
							
						}
						//System.out.println("BATIMENTO = "+batimento);
						pacienteWidget.updateData("batimento", batimento);
						
						
						// set color to represent brightness level
						setBackground(new Color(batimento,batimento,batimento));
					}
				});
				setOpaque(true); // to allow background color to show
				setMajorTickSpacing(50);
				setPaintTicks(true);
				setPaintLabels(true);
			}});
			
			/*
			 * UI for presence
			 */
			
			add(new JLabel("Presenca") {{ setFont(getFont().deriveFont(fontSize)); }});
			
			add(presencaSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1, 1)) {{ // # people from 0 to 5 (parametro alterado para presenca = 1 ou presenca = 0)
				// get text field to color text
				final JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor) getEditor();
				final JFormattedTextField tf = editor.getTextField();
				tf.setForeground(Color.red);
				setFont(getFont().deriveFont(fontSize));
				
				addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent evt) {
						String nome = tf.getText();
						
						System.out.println("NOME = "+nome); 
						int presenca = (Integer) presencaSpinner.getValue();
						pacienteWidget.updateData("presenca", presenca);
						
						// color text red for when presence is red
						
						if (presenca == 0) { tf.setForeground(Color.red); }
						else { tf.setForeground(Color.blue);}
					}
				});
			}});
			
			// UI for light level
			add(new JLabel("Email") {{ setFont(getFont().deriveFont(fontSize)); }});
			add(emailLabel);
			
			/*
			 * Init state of widgets
			 */
			short pressao = (short)pressaoSlider.getValue();
			short glicose = (short)glicoseSlider.getValue();
			short batimento = (short)batimentoSlider.getValue();
			int presenca = (Integer) presencaSpinner.getValue();
			pacienteWidget.updateData("pressao", pressao);
			pacienteWidget.updateData("presenca", presenca);
			pacienteWidget.updateData("glicose", glicose);
			pacienteWidget.updateData("batimento", batimento);
			//nivelGlicoseWidget.updateData("glicose", glicose);			
		}
		
		
		
		public JSlider getBatimentos(){
			return batimentoSlider;		
		}
		
		public JSlider getPressao(){
			return pressaoSlider;		
		}
		
		public JSlider getGlicose(){
			return glicoseSlider;		
		}
		
		public JSpinner getPresenca(){
			return presencaSpinner;		
		}
		
	}
	
	public MonitoramentoDePacienteUI getUI(){
		return ui;	
	}
	
	/*public Widget getPaciente(){
		return pacienteWidget;
	}
	*/
	public JFrame getPaciente(){
		return paciente;
	}
	
	
	
		
	public static void main(String[] args) {
		Discoverer.start();
		
		MonitoramentoDePaciente app = new MonitoramentoDePaciente();
		
		/*
		 * GUI frame
		 */
		paciente  = new JFrame("Z�");
		paciente.add(app.ui);
		paciente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		paciente.setSize(new Dimension(1000, 600));
		paciente.setLocationRelativeTo(null); // center of screen
		paciente.setVisible(true);	
		
	}

}
