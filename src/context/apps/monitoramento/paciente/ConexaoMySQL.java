package context.apps.monitoramento.paciente;
//package com.mysql.*;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ConexaoMySQL {

	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost/monitoramento";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "root";

	/*public static void main(String[] argv) {

		try {

			insertRecordIntoTable();

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		}

	}
*/
	public static void insertRecordIntoTable(String nome, int batimento, int glicose, int pressao ) throws SQLException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO pacientes "
				+ "(nome, batimento, glicose, pressao, dataHora) VALUES "
				+ "(?,?,?,?,?)";

		try {
			dbConnection = getDBConnection();
			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, nome);
			preparedStatement.setInt(2, batimento);
			preparedStatement.setInt(3, glicose);
			preparedStatement.setInt(4, pressao);
			preparedStatement.setTimestamp(5, getCurrentTimeStamp());

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			System.out.println("Record is inserted into DBUSER table!");

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

	}


	public static void updateTable(String nome, String nome2 ) throws SQLException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "UPDATE pacientes SET nome=? WHERE nome=?";

		try {
			dbConnection = getDBConnection();
			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, nome2);
			preparedStatement.setString(2, nome);

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			System.out.println("Atualizando dados da tabela!");

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

	}


	public static void deleteTable(String nome) throws SQLException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "DELETE FROM pacientes WHERE nome=?";

		try {
			dbConnection = getDBConnection();
			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, nome);

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			System.out.println("Atualizando dados da tabela!");

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

	}


	public static void selectTable(String nome) throws SQLException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "SELECT * FROM paciente WHERE nome=?";

		try {
			dbConnection = getDBConnection();
			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, nome);
			ResultSet result = preparedStatement.executeQuery(insertTableSQL);
			//int count = 0;
			 
			while (result.next()){
				String id = result.getString(1);
			    String name = result.getString(2);
			    String batimento = result.getString(3);
			 
			    String output = "Pacientes #%d: %s - %s";
			    System.out.println(String.format(output, id, name, batimento));
			}

			// execute insert SQL stetement

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

	}

	public static Connection getDBConnection() {

		Connection dbConnection = null;

		try {

			Class.forName(DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());

		}

		try {

			dbConnection = DriverManager.getConnection(
                            DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		}

		return dbConnection;

	}
	
	public static boolean FecharConexao() {

		try {
			ConexaoMySQL.getDBConnection().close();
			return true;
		} catch (SQLException e) {
			return false;
		}

	}
	
	public static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}

}