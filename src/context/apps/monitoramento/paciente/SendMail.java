package context.apps.monitoramento.paciente;
//package crunchify.com.tutorial;
 
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 

/**
 * @author Crunchify.com
 * 
 */
 
public class SendMail {
 
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
 
	public static void main(String args[]) throws AddressException, MessagingException, SQLException {
		//generateAndSendEmail();
		System.out.println("\n\n ===> Your Java Program has just sent an Email successfully. Check your email..");
	}
 // throws AddressException, MessagingException, SQLException, IOException
	public static void generateAndSendEmail(String nome, int batimento, int glicose, int pressao) throws AddressException, MessagingException, SQLException{
		
		
		//
		
		ConexaoMySQL con = new ConexaoMySQL();
		con.getDBConnection();
		
		con.insertRecordIntoTable(nome,batimento,glicose,pressao);
		con.FecharConexao();
		
				
		//
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		mailServerProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com");


		System.out.println("Mail Server Properties have been setup successfully..");
 
		//
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		// emails do gmail
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("juc@lcc.ufrn.br"));
		generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("juc@lcc.ufrn.br"));
		generateMailMessage.setSubject("Teste de email - sistema de monitoramento. ");
		String emailBody = "ALERTA - Sistema de Monitoramento. " + 
				"<br><br> O paiciente " + nome + " Est� com sinais vitais abaixo do normal"
						+ "<br>Resumo:<br> <b> Batimentos Card�acos = </b> "+
						batimento +"<b> N�vel de Glicose = </b> "+
						glicose +"<b> Press�o Arterial = </b> "+
						pressao;
		generateMailMessage.setContent(emailBody, "text/html");
		System.out.println("Mail Session has been created successfully..");
 
		// 
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");
 
		//coloque um email do gmail e sua senha (remetente)
		transport.connect("smtp.gmail.com", "cardoso.juliano@gmail.com", "Slipknot8");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
	}
}