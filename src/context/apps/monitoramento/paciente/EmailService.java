package context.apps.monitoramento.paciente;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import context.arch.comm.DataObject;
import context.arch.service.Service;
import context.arch.service.helper.FunctionDescription;
import context.arch.service.helper.FunctionDescriptions;
import context.arch.service.helper.ServiceInput;
import context.arch.widget.Widget;

/**
 * Custom service for the room application to set the light level.
 * @author Brian Y. Lim
 *
 */
public class EmailService extends Service {
	
	// package protected to be accessible to UI of HelloRoom app
	JLabel emailLabel;

	@SuppressWarnings("serial")
	public EmailService(final Widget widget) {
		super(widget, "EmailService", 
				new FunctionDescriptions() {
					{ // constructor
						// define function for the service
						add(new FunctionDescription(
								"emailControl", 
								"Sets the light level of the lamp", 
								widget.getNonConstantAttributes()));
					}
				});
		
		/*
		 * set up light label (for use in a UI)
		 */
		emailLabel = new JLabel("0") {{
			setHorizontalAlignment(JLabel.RIGHT);
			setBorder(BorderFactory.createEtchedBorder());
			
			setOpaque(true); // to allow background color to show
			// set color to represent light level
			setBackground(Color.black); // initially dark
		}};
	}

	@Override
	public DataObject execute(ServiceInput serviceInput) {
		int email = serviceInput.getInput().getAttributeValue("email");
		// light is from 0 to 10
		emailLabel.setText(String.valueOf(email));
		
		//serviceInput.get
		//emailLabel.setText(String.valueOf(56));
		emailLabel.setBackground(new Color(email*25,email*23, email*16));
		
		return new DataObject(); // no particular info to return
	}

}
